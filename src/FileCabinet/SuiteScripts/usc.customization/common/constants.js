define([], function () {
    return Object.freeze({
        CUSTOM_RECORDS: {
            CHECK_IN: 'customrecord_pdg_check_in',
            CHECK_IN_MAPPING_REG_CODE: 'customrecord_pdg_check_in_map_reg_code',
            VENDOR_PRICE_LIST: 'customrecord_pdg_vendor_price_list',
        },
        CUSTOM_FIELDS: {
            CHECK_IN_MAPPING_REG_CODE: {
                REGION_CODE_SOURCE: 'custrecord_pdg_region_code_source',
                REGION_CODE_TARGET: 'custrecord_pdg_region_code_target',
            },
            CHECK_IN: {
                REGION_CODE: 'custrecord_pdg_region_code',
                REGION_CODE_L: 'custrecord_pdg_region_code_l',
                MEMBER_ID: 'custrecord_pdg_member_id',
                CLASS: 'custrecord_pdg_class',
                PRICE_LEVEL: 'custrecord_pdg_price_level',
                STUDIO_ID: 'custrecord_pdg_studio_id',
                LOCATION_ID: 'custrecord_pdg_location_id',
                MERCHANT_ID: 'custrecord_pdg_merchant_id',
                MERCHANT_ID_LIST: 'custrecord_pdg_merchant_id_l',
                APPOINTMENT_TIME: 'custrecord_pdg_appointment_time',
                BOOKING_STATE: 'custrecord_pdg_booking_state',
                BOOKING_NET_COST: 'custrecord_pdg_booking_net_cost',
                SPORT_ITEM: 'custrecord_pdg_sport_item',
                CHECK_IN_PROCESSED: 'custrecord_pdg_check_in_processed',
                MAPPING_FAILED: 'custrecord_pdg_mapping_failed',
                PO_VENDOR: 'custrecord_pdg_po_vendor',
                PROCESSING_ERROR: 'custrecord_pdg_processing_error',
                GENERATED_TRANSACTION: 'custrecord_pdg_generated_transaction',
            },
            VENDOR_PRICE_LIST: {
                ITEM: 'custrecord_pdg_vpl_item',
                VENDOR: 'custrecord_pdg_vpl_vendor',
                PRICE: 'custrecord_pdg_vpl_price',
                PRICE_LEVEL: 'custrecord_pdg_vpl_price_level',
            },
            ENTITY: {
                CLASS: 'custentity_usc_customer_class',
            },
            OTHER: {
                CHECK_IN_ID: 'check_in_id',
                ITEMS: 'items',
                QUANTITY: 'quantity'
            },
        },
        CUSTOM_COLUMNS: {
            LOCATION_ID: 'custcol_pdg_location_id',
        },
        CUSTOM_SEGMENTS: {
            REGION: 'cseg_usc_region',
        },
        SUITELET: {
            LABELS: {
                FORM: {
                    TITLE: 'Process Check-In Records'
                },
                BUTTONS: {
                    SUBMIT: 'Generate Transactions',
                },
                FIELDS: {
                    DATE: 'Date',
                    POSTING_PERIOD: 'Posting Period',
                    SUBSIDIARY: 'Subsidiary',
                    PURCHASE_ORDERS: 'Purchase Orders',
                    VENDOR_BILLS: 'Vendor Bills',
                    VENDOR: 'Vendor',
                },
            },
            IDS: {
                FIELDS: {
                    DATE: 'custpage_date',
                    POSTING_PERIOD: 'custpage_posting_period',
                    SUBSIDIARY: 'custpage_subsidiary',
                    PURCHASE_ORDERS: 'custpage_purchase_orders',
                    VENDOR_BILLS: 'custpage_vendor_bills',
                    VENDOR: 'custpage_vendor',
                    STATUS: 'custpage_status',
                },
            },
            MESSAGES: {
                MR_SCRIPT_IS_RUNNING: 'Map/Reduce script which generates CSV files is currently running. Please try again later.',
            },
        },
        SCRIPTS: {
            MR_GENERATE_TRANSACTIONS: {
                SCRIPT_ID: 'customscript_pdg_mr_generate_pos_vbs',
                SCRIPT_DEPLOYMENT_ID: 'customdeploy_pdg_mr_generate_pos_vbs',
            },
        },
        SCRIPT_PARAMS: {
            MR_GENERATE_TRANSACTIONS: {
                DATE: 'custscript_date',
                POSTING_PERIOD: 'custscript_posting_period',
                SUBSIDIARY_FILTER: 'custscript_subsidiary_filter',
                VENDOR_FILTER: 'custscript_vendor_filter',
                PURCHASE_ORDERS_FILTER: 'custscript_purchase_orders_filter',
                VENDOR_BILLS_FILTER: 'custscript_vendor_bills_filter',
                CHECK_IN_SEARCH: 'custscript_check_ins_for_processing',
            },
            UE_VALIDATE_MAPPING: {
                MERCHANT_CATEGORY: 'custscript_merchant_category',
            },
        },
    });
});