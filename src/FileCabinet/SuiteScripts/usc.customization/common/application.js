define(['N/format', 'N/email', '../common/constants'], function (format, email, constants) {
    const getFormattedDate = (inputDate) => {

        let formattedDate = format.parse({
            value: inputDate,
            type: format.Type.DATE
        });

        return formattedDate;
    };

    const isDefined = (value) => {
        return value !== null && value !== undefined && value !== '' && !isNaN(value);
    }

    return {
        getFormattedDate: getFormattedDate,
        isDefined, isDefined,
    };
});