define(
    ['N/search', '../common/constants'],
    function (search, constants) {

        function getVendorId (vendorName, merchantCategory) {
            let vendorSearch = search.create({
                type: search.Type.VENDOR,
                filters: [
                    ['entityid', 'is', vendorName],
                    'AND',
                    ['category', 'anyof', merchantCategory],
                ],
                columns: [
                    'internalid',
                ]
            });

            let vendorId = null;

            vendorSearch.run().each(function(result) {
                vendorId = result.getValue('internalid');
            });

            return vendorId;
        }

        return {
            getVendorId,
        };
    }
);