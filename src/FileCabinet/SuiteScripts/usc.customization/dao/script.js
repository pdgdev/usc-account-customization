define(
    ['N/search', '../common/constants'],
    function (search, constants) {

        function isMapReduceScriptRunning () {
            let scriptDeploymentSearch = search.create({
                type: search.Type.SCHEDULED_SCRIPT_INSTANCE,
                filters:
                    [
                        ['script.scriptid', 'is', constants.SCRIPTS.MR_GENERATE_TRANSACTIONS.SCRIPT_ID],
                        'AND',
                        ['status', 'anyof', 'PENDING', 'PROCESSING']
                    ],
                columns:
                    [
                        'taskid'
                    ]
            });

            let isRunning = false;

            scriptDeploymentSearch.run().each(function(result)
            {
                isRunning = true;
            });

            return isRunning;
        }

        function getMapReduceScriptId() {
            let filter = search.createFilter({
                name: 'scriptid',
                operator: search.Operator.IS,
                values : [constants.SCRIPTS.MR_GENERATE_TRANSACTIONS.SCRIPT_ID],
            });
            let scriptSearch = search.create({
                type: search.Type.MAP_REDUCE_SCRIPT,
            });

            scriptSearch.filters = scriptSearch.filters.concat(filter);

            let scriptId;

            scriptSearch.run().each(function(result) {
                scriptId = result.id;
            });

            return scriptId;
        }

        return {
            isMapReduceScriptRunning,
            getMapReduceScriptId,
        };
    }
);