define(
    ['N/search', 'N/record'],
    function (search, record) {

        function getCustomerRecord (customerName) {
            let customerSearch = search.create({
                type: search.Type.CUSTOMER,
                filters: [
                    ['entityid', 'is', customerName]
                ],
                columns: [
                    'internalid'
                ]
            });

            let customerId = null;

            customerSearch.run().each(function(result) {
                customerId = result.getValue('internalid');
            });

            if (customerId) {
                return record.load({
                    type: record.Type.CUSTOMER,
                    id: customerId
                });
            }
        }

        return {
            getCustomerRecord,
        };
    }
);