define(
    ['N/search', '../common/constants'],
    function (search, constants) {

        function getRegionCodeId (regionCode) {
            let regionCodeSearch = search.create({
                type: constants.CUSTOM_RECORDS.CHECK_IN_MAPPING_REG_CODE,
                filters: [
                    [constants.CUSTOM_FIELDS.CHECK_IN_MAPPING_REG_CODE.REGION_CODE_SOURCE, 'is', regionCode]
                ],
                columns: [
                    constants.CUSTOM_FIELDS.CHECK_IN_MAPPING_REG_CODE.REGION_CODE_TARGET
                ]
            });

            let regionCodeId = null;

            regionCodeSearch.run().each(function(result) {
                regionCodeId = result.getValue(constants.CUSTOM_FIELDS.CHECK_IN_MAPPING_REG_CODE.REGION_CODE_TARGET);
            });

            return regionCodeId;
        }

        function getItemPrice (itemId, vendorId, priceLevel) {

            if (!itemId || !vendorId || !priceLevel) {
                return;
            }

            let vendorPriceListSearch = search.create({
                type: constants.CUSTOM_RECORDS.VENDOR_PRICE_LIST,
                filters: [
                    [constants.CUSTOM_FIELDS.VENDOR_PRICE_LIST.ITEM, 'is', itemId],
                    'AND',
                    [constants.CUSTOM_FIELDS.VENDOR_PRICE_LIST.VENDOR, 'is', vendorId],
                    'AND',
                    [constants.CUSTOM_FIELDS.VENDOR_PRICE_LIST.PRICE_LEVEL, 'is', priceLevel],
                ],
                columns: [
                    constants.CUSTOM_FIELDS.VENDOR_PRICE_LIST.PRICE
                ]
            });

            let price = null;

            vendorPriceListSearch.run().each(function(result) {
                price = result.getValue(constants.CUSTOM_FIELDS.VENDOR_PRICE_LIST.PRICE);
            });

            return price;
        }

        return {
            getRegionCodeId,
            getItemPrice,
        }
    }
);