/**
 * @NApiVersion 2.1
 */
define([
        'N/task',
        '../common/constants',
    ],
    (
        task,
        constants,
    ) => {

        function ScriptScheduler (scriptId, scriptDeploymentId) {
            this.scriptId = scriptId;
            this.scriptDeploymentId = scriptDeploymentId;
            this.scriptParameters = {};
            this.mapReduceTask = null;
        }

        ScriptScheduler.prototype.addScriptParameter = function (name, value) {
            this.scriptParameters[name] = value;

            return this;
        };

        ScriptScheduler.prototype.createMapReduceTask = function () {
            this.mapReduceTask = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: this.scriptId,
                deploymentId: this.scriptDeploymentId,
                params: this.scriptParameters
            });

            return this;
        }

        ScriptScheduler.prototype.submitMapReduceTask = function () {
            this.mapReduceTask.submit();
        }

        return {Instance: ScriptScheduler};
    });