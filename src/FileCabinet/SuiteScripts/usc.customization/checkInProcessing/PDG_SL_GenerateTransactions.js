/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 */
define(['N/record', './CheckInSuiteletBuilder', './ScriptScheduler', '../common/constants', '../dao/script'],
    /**
 * @param{record} record
 */
    (record, CheckInSuiteletBuilder, ScriptScheduler, constants, scriptDAO) => {
        /**
         * Defines the Suitelet script trigger point.
         * @param {Object} scriptContext
         * @param {ServerRequest} scriptContext.request - Incoming request
         * @param {ServerResponse} scriptContext.response - Suitelet response
         * @since 2015.2
         */
        const onRequest = (scriptContext) => {
            let request = scriptContext.request;
            let response = scriptContext.response;

            let suiteletBuilder = new CheckInSuiteletBuilder.Instance()
                .createForm();

            if (request.method === 'GET') {

                if (scriptDAO.isMapReduceScriptRunning()) {
                    suiteletBuilder
                        .addScriptIsRunningText()
                        .displayForm(response);
                }
                else {
                    suiteletBuilder
                        .addFilterFields()
                        .addSubmitButton()
                        .displayForm(response);
                }
            }
            else {
                let date = request.parameters[constants.SUITELET.IDS.FIELDS.DATE];
                let postingPeriod = request.parameters[constants.SUITELET.IDS.FIELDS.POSTING_PERIOD];
                let subsidiaryId = request.parameters[constants.SUITELET.IDS.FIELDS.SUBSIDIARY];
                let vendorId = request.parameters[constants.SUITELET.IDS.FIELDS.VENDOR];
                let generatePurchaseOrders = request.parameters[constants.SUITELET.IDS.FIELDS.PURCHASE_ORDERS];
                let generateVendorBills = request.parameters[constants.SUITELET.IDS.FIELDS.VENDOR_BILLS];

                let scriptScheduler = new ScriptScheduler.Instance(constants.SCRIPTS.MR_GENERATE_TRANSACTIONS.SCRIPT_ID,
                    constants.SCRIPTS.MR_GENERATE_TRANSACTIONS.SCRIPT_DEPLOYMENT_ID)
                    .addScriptParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.DATE, date)
                    .addScriptParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.POSTING_PERIOD, postingPeriod)
                    .addScriptParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.SUBSIDIARY_FILTER, subsidiaryId)
                    .addScriptParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.VENDOR_FILTER, vendorId)
                    .addScriptParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.PURCHASE_ORDERS_FILTER, generatePurchaseOrders)
                    .addScriptParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.VENDOR_BILLS_FILTER, generateVendorBills)
                    .createMapReduceTask()
                    .submitMapReduceTask();

                suiteletBuilder.redirectToMapReduceScriptStatus();
            }
        }

        return {onRequest}

    });
