/**
 * @NApiVersion 2.1
 */
define([
        'N/record',
        'N/format',
        '../common/constants',
    ],
    (
        record,
        format,
        constants
    ) => {

        function TransactionBuilder () {
            this.transaction = null;
        }

        TransactionBuilder.prototype.createPurchaseOrder = function () {
            this.transaction = record.create({
                type: record.Type.PURCHASE_ORDER,
                isDynamic: true,
            });

            return this;
        };

        TransactionBuilder.prototype.createVendorBill = function () {
            this.transaction = record.create({
                type: record.Type.VENDOR_BILL,
                isDynamic: true,
            });

            return this;
        };

        TransactionBuilder.prototype.setAllFields = function (values) {
            this.transaction.setValue({
                fieldId: 'entity',
                value: values[constants.CUSTOM_FIELDS.CHECK_IN.MERCHANT_ID_LIST],
            });

            for (let i = 0; i < values[constants.CUSTOM_FIELDS.OTHER.ITEMS].length; i++) {
                let lineData = values[constants.CUSTOM_FIELDS.OTHER.ITEMS][i];
                this._createTransactionLine(lineData[constants.CUSTOM_FIELDS.CHECK_IN.SPORT_ITEM],
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.BOOKING_NET_COST],
                    lineData[constants.CUSTOM_FIELDS.OTHER.QUANTITY],
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.REGION_CODE_L],
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.CLASS],
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.LOCATION_ID]);
            }

            return this;
        }

        TransactionBuilder.prototype.setTransactionDate = function (date) {

            let formattedDate = format.parse({
                value: date,
                type: format.Type.DATE
            });

            this.transaction.setValue({
                fieldId: 'trandate',
                value: formattedDate,
            });

            return this;
        }

        TransactionBuilder.prototype.saveTransaction = function () {
            return this.transaction.save();
        };

        TransactionBuilder.prototype._createTransactionLine = function (itemId, rate, quantity, regionId, classId, locationId) {
            this.transaction.selectNewLine({
                sublistId: 'item',
            });

            this.transaction.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item',
                value: itemId,
            });

            this.transaction.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'rate',
                value: rate,
            });

            this.transaction.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'quantity',
                value: quantity,
            });

            this.transaction.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: constants.CUSTOM_SEGMENTS.REGION,
                value: regionId,
            });

            this.transaction.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'class',
                value: classId,
            });

            this.transaction.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: constants.CUSTOM_COLUMNS.LOCATION_ID,
                value: locationId,
            });

            this.transaction.commitLine({
                sublistId: 'item',
            });
        }

        return {Instance: TransactionBuilder};
    });
