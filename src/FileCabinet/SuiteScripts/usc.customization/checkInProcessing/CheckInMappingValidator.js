/**
 * @NApiVersion 2.1
 */
define([
        'N/record',
        '../common/constants',
        '../common/application',
        '../dao/checkInMapping',
        '../dao/vendor',
        '../dao/customer',
    ],
    (
        record,
        constants,
        app,
        checkInMappingDAO,
        vendorDAO,
        customerDAO
    ) => {

        function CheckInMappingValidator (checkInRecord) {
            this.checkInRecord = checkInRecord;
            this.item = checkInRecord.getValue(constants.CUSTOM_FIELDS.CHECK_IN.SPORT_ITEM);
            this.merchantCategory = null;
            this.regionCodeId = null;
            this.vendorId = null;
            this.class = null;
            this.priceLevel = null;
            this.price = null;
            this.customer = null;
            this.mappingFailed = null;
        }

        CheckInMappingValidator.prototype.setMerchantCategory = function (merchantCategory) {
            this.merchantCategory = merchantCategory;

            return this;
        };

        CheckInMappingValidator.prototype.searchAndInitRecordFields = function () {
            this._getRegionCodeId();
            this._getVendorId();
            this._getCustomer();

            if (this.customer) {
                this.class = this.customer.getValue(constants.CUSTOM_FIELDS.ENTITY.CLASS);
                this.priceLevel = this.customer.getValue('pricelevel');

                this._getItemPrice();
            }

            return this;
        };

        CheckInMappingValidator.prototype.validateRecordFields = function () {
            this.mappingFailed = !app.isDefined(this.regionCodeId) || !app.isDefined(this.item) ||
                !app.isDefined(this.class) || !app.isDefined(this.priceLevel) || !app.isDefined(this.price);

            return this;
        };

        CheckInMappingValidator.prototype.setCheckInFields = function () {
            this.checkInRecord.setValue({
                fieldId: constants.CUSTOM_FIELDS.CHECK_IN.REGION_CODE_L,
                value: this.regionCodeId,
            });

            this.checkInRecord.setValue({
                fieldId: constants.CUSTOM_FIELDS.CHECK_IN.MERCHANT_ID_LIST,
                value: this.vendorId,
            });

            this.checkInRecord.setValue({
                fieldId: constants.CUSTOM_FIELDS.CHECK_IN.CLASS,
                value: this.class,
            });

            this.checkInRecord.setValue({
                fieldId: constants.CUSTOM_FIELDS.CHECK_IN.PRICE_LEVEL,
                value: this.priceLevel,
            });

            this.checkInRecord.setValue({
                fieldId: constants.CUSTOM_FIELDS.CHECK_IN.BOOKING_NET_COST,
                value: this.price,
            });

            this.checkInRecord.setValue({
                fieldId: constants.CUSTOM_FIELDS.CHECK_IN.MAPPING_FAILED,
                value: this.mappingFailed,
            });

            return this;
        };

        CheckInMappingValidator.prototype._getRegionCodeId = function () {
            let regionCode = this.checkInRecord.getValue(constants.CUSTOM_FIELDS.CHECK_IN.REGION_CODE);
            this.regionCodeId = checkInMappingDAO.getRegionCodeId(regionCode);
        };

        CheckInMappingValidator.prototype._getVendorId = function () {
            let vendorName = this.checkInRecord.getValue(constants.CUSTOM_FIELDS.CHECK_IN.MERCHANT_ID);
            this.vendorId = vendorDAO.getVendorId(vendorName, this.merchantCategory);
        };

        CheckInMappingValidator.prototype._getCustomer = function () {
            let customerName = this.checkInRecord.getValue(constants.CUSTOM_FIELDS.CHECK_IN.MEMBER_ID);
            this.customer = customerDAO.getCustomerRecord(customerName);
        };

        CheckInMappingValidator.prototype._getItemPrice = function () {
            this.price = checkInMappingDAO.getItemPrice(this.item, this.vendorId, this.priceLevel);
        };

        return {Instance: CheckInMappingValidator};
    });