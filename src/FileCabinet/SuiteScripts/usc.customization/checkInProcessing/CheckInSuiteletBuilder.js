/**
 * @NApiVersion 2.1
 */
define([
        'N/record',
        'N/ui/serverWidget',
        'N/redirect',
        '../common/constants',
        '../dao/script',
    ],
    (
        record,
        ui,
        redirect,
        constants,
        scriptDAO
    ) => {

        function CheckInSuiteletBuilder () {
            this.checkInSuiteletForm = null;
        }

        CheckInSuiteletBuilder.prototype.createForm = function () {
            this.checkInSuiteletForm = ui.createForm({
                title: constants.SUITELET.LABELS.FORM.TITLE,
            });

            return this;
        };

        CheckInSuiteletBuilder.prototype.addSubmitButton = function () {
            this.checkInSuiteletForm.addSubmitButton({
                label: constants.SUITELET.LABELS.BUTTONS.SUBMIT,
            });

            return this;
        };

        CheckInSuiteletBuilder.prototype.addFilterFields = function () {
            this.checkInSuiteletForm.addField({
                id: constants.SUITELET.IDS.FIELDS.DATE,
                type: ui.FieldType.DATE,
                label: constants.SUITELET.LABELS.FIELDS.DATE,
            }).isMandatory = true;

            this.checkInSuiteletForm.addField({
                id: constants.SUITELET.IDS.FIELDS.SUBSIDIARY,
                type: ui.FieldType.SELECT,
                source: record.Type.SUBSIDIARY,
                label: constants.SUITELET.LABELS.FIELDS.SUBSIDIARY,
            });

            this.checkInSuiteletForm.addField({
                id: constants.SUITELET.IDS.FIELDS.VENDOR,
                type: ui.FieldType.SELECT,
                source: record.Type.VENDOR,
                label: constants.SUITELET.LABELS.FIELDS.VENDOR,
            });

            this.checkInSuiteletForm.addField({
                id: constants.SUITELET.IDS.FIELDS.PURCHASE_ORDERS,
                type: ui.FieldType.CHECKBOX,
                label: constants.SUITELET.LABELS.FIELDS.PURCHASE_ORDERS,
            }).defaultValue = 'T';

            this.checkInSuiteletForm.addField({
                id: constants.SUITELET.IDS.FIELDS.VENDOR_BILLS,
                type: ui.FieldType.CHECKBOX,
                label: constants.SUITELET.LABELS.FIELDS.VENDOR_BILLS,
            }).defaultValue = 'T';

            return this;
        };

        CheckInSuiteletBuilder.prototype.addScriptIsRunningText = function () {
            this.checkInSuiteletForm.addField({
                id: constants.SUITELET.IDS.FIELDS.STATUS,
                type: ui.FieldType.LABEL,
                label: constants.SUITELET.MESSAGES.MR_SCRIPT_IS_RUNNING,
            });

            return this;
        };

        CheckInSuiteletBuilder.prototype.redirectToMapReduceScriptStatus = function () {
            let scriptId = scriptDAO.getMapReduceScriptId();
            redirect.toTaskLink({
                id: 'LIST_MAPREDUCESCRIPTSTATUS',
                parameters: {'sortcol':'dcreated',
                    'sortdir':'DESC',
                    'datemodi':'WITHIN',
                    'daterange':'TODAY',
                    'date':'TODAY',
                    'scripttype': scriptId,
                    'primarykey': '',
                },
            });
        };

        CheckInSuiteletBuilder.prototype.displayForm = function (response) {
            response.writePage(this.checkInSuiteletForm);
        };

        return {Instance: CheckInSuiteletBuilder};
    });
