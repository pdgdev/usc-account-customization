/**
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/record', 'N/runtime', 'N/search', 'N/format', '../common/constants', './TransactionBuilder'],
    /**
 * @param{runtime} runtime
 * @param{search} search
 */
    (record, runtime, search, format, constants, TransactionBuilder) => {
        /**
         * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
         * @param {Object} inputContext
         * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
         *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
         * @param {Object} inputContext.ObjectRef - Object that references the input data
         * @typedef {Object} ObjectRef
         * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
         * @property {string} ObjectRef.type - Type of the record instance that contains the input data
         * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
         * @since 2015.2
         */
        const getInputData = (inputContext) => {
            let scriptObj = runtime.getCurrentScript();
            let checkInSearchId = scriptObj.getParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.CHECK_IN_SEARCH);
            let subsidiaryId = scriptObj.getParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.SUBSIDIARY_FILTER);
            let vendorId = scriptObj.getParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.VENDOR_FILTER);
            let generatePurchaseOrders = scriptObj.getParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.PURCHASE_ORDERS_FILTER);
            let generateVendorBills = scriptObj.getParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.VENDOR_BILLS_FILTER);

            let checkInSearch = search.load({
                id: checkInSearchId,
            });

            if (generatePurchaseOrders === 'F' && generateVendorBills === 'T') {
                let transactionTypeFilter = search.createFilter({
                    name: constants.CUSTOM_FIELDS.CHECK_IN.PO_VENDOR,
                    operator: search.Operator.IS,
                    values: false
                });
                checkInSearch.filters = checkInSearch.filters.concat(transactionTypeFilter);
            }
            else if (generatePurchaseOrders === 'T' && generateVendorBills === 'F') {
                let transactionTypeFilter = search.createFilter({
                    name: constants.CUSTOM_FIELDS.CHECK_IN.PO_VENDOR,
                    operator: search.Operator.IS,
                    values: true
                });
                checkInSearch.filters = checkInSearch.filters.concat(transactionTypeFilter);
            }
            else if (generatePurchaseOrders === 'F' && generateVendorBills === 'F') {
                return;
            }

            if (subsidiaryId) {
                let subsidiaryFilter = search.createFilter({
                    name: 'subsidiary',
                    join: constants.CUSTOM_FIELDS.CHECK_IN.MERCHANT_ID_LIST,
                    operator: search.Operator.ANYOF,
                    values: subsidiaryId
                });
                checkInSearch.filters = checkInSearch.filters.concat(subsidiaryFilter);
            }

            if (vendorId) {
                let vendorFilter = search.createFilter({
                    name: constants.CUSTOM_FIELDS.CHECK_IN.MERCHANT_ID_LIST,
                    operator: search.Operator.ANYOF,
                    values: vendorId
                });
                checkInSearch.filters = checkInSearch.filters.concat(vendorFilter);
            }

            return checkInSearch;
        }

        /**
         * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
         * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
         * context.
         * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
         *     is provided automatically based on the results of the getInputData stage.
         * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
         *     function on the current key-value pair
         * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
         *     pair
         * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
         *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
         * @param {string} mapContext.key - Key to be processed during the map stage
         * @param {string} mapContext.value - Value to be processed during the map stage
         * @since 2015.2
         */
        const map = (mapContext) => {
            let checkInRecordId = mapContext.key;
            let values = JSON.parse(mapContext.value).values;
            values[constants.CUSTOM_FIELDS.OTHER.CHECK_IN_ID] = checkInRecordId;

            if (appointmentDateIsInSelectedMonth(values[constants.CUSTOM_FIELDS.CHECK_IN.APPOINTMENT_TIME])) {
                let vendorId = values[constants.CUSTOM_FIELDS.CHECK_IN.MERCHANT_ID_LIST].value;

                mapContext.write({
                    key: vendorId,
                    value: values
                });
            }
        }

        /**
         * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
         * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
         * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
         *     provided automatically based on the results of the map stage.
         * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
         *     reduce function on the current group
         * @param {number} reduceContext.executionNo - Number of times the reduce function has been executed on the current group
         * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
         *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
         * @param {string} reduceContext.key - Key to be processed during the reduce stage
         * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
         *     for processing
         * @since 2015.2
         */
        const reduce = (reduceContext) => {
            let LOGGING_TITLE = '<< reduce >>';

            let transactionData = getReadableDataWithMergedLines(reduceContext.values);

            let updateValues = {};

            let scriptObject = runtime.getCurrentScript();
            let date = scriptObject.getParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.DATE);

            try {
                let transactionBuilder = new TransactionBuilder.Instance();

                if (transactionData[constants.CUSTOM_FIELDS.CHECK_IN.PO_VENDOR] === 'T') {
                    transactionBuilder.createPurchaseOrder();
                }
                else {
                    transactionBuilder.createVendorBill();
                }

                let transactionId = transactionBuilder
                    .setAllFields(transactionData)
                    .setTransactionDate(date)
                    .saveTransaction();

                updateValues[constants.CUSTOM_FIELDS.CHECK_IN.GENERATED_TRANSACTION] = transactionId;
                updateValues[constants.CUSTOM_FIELDS.CHECK_IN.PROCESSING_ERROR] = '';

                log.debug(LOGGING_TITLE, 'Transaction has been created. ID #' + transactionId);
            }
            catch (error) {
                log.error(LOGGING_TITLE, error);
                updateValues[constants.CUSTOM_FIELDS.CHECK_IN.PROCESSING_ERROR] = error.message;
            }

            updateValues[constants.CUSTOM_FIELDS.CHECK_IN.CHECK_IN_PROCESSED] = true;
            log.debug('<< reduce >>', 'updateValues: ' + JSON.stringify(updateValues));
            log.debug('<< reduce >>', 'check-in IDs: ' + JSON.stringify(transactionData[constants.CUSTOM_FIELDS.OTHER.CHECK_IN_ID]));

            for (let i = 0; i < transactionData[constants.CUSTOM_FIELDS.OTHER.CHECK_IN_ID].length; i++) {
                record.submitFields({
                    type: constants.CUSTOM_RECORDS.CHECK_IN,
                    id: transactionData[constants.CUSTOM_FIELDS.OTHER.CHECK_IN_ID][i],
                    values: updateValues
                });
            }
        }

        /**
         * Converts context data into the object with separated header fields and array of grouped line item fields.
         * @param contextData
         * @returns {object}
         */
        const getReadableDataWithMergedLines = (contextData) => {
            let headerData = JSON.parse(contextData[0]);
            let linesParsedData = getLinesParsedData(contextData);

            let transactionData = {};
            transactionData[constants.CUSTOM_FIELDS.CHECK_IN.MERCHANT_ID_LIST] = headerData[constants.CUSTOM_FIELDS.CHECK_IN.MERCHANT_ID_LIST].value;
            transactionData[constants.CUSTOM_FIELDS.CHECK_IN.APPOINTMENT_TIME] = headerData[constants.CUSTOM_FIELDS.CHECK_IN.APPOINTMENT_TIME].value;
            transactionData[constants.CUSTOM_FIELDS.CHECK_IN.PO_VENDOR] = headerData[constants.CUSTOM_FIELDS.CHECK_IN.PO_VENDOR];
            transactionData[constants.CUSTOM_FIELDS.OTHER.ITEMS] = getMergedLines(linesParsedData);
            transactionData[constants.CUSTOM_FIELDS.OTHER.CHECK_IN_ID] = getCheckInIDs(linesParsedData);

            return transactionData;
        }

        /**
         * Iterates over search results and group them based on certain values. Each grouped result represents 1 line with certain quantity.
         * @param linesParsedData
         * @returns [array]
         */
        const getMergedLines = (linesParsedData) => {
            let mergedLines = [];

            for (let i = 0; i < linesParsedData.length; i++) {
                let lineFound = false;

                for (let j = 0; j < mergedLines.length; j++) {
                    if (linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.REGION_CODE_L].value === mergedLines[j][constants.CUSTOM_FIELDS.CHECK_IN.REGION_CODE_L] &&
                        linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.STUDIO_ID] === mergedLines[j][constants.CUSTOM_FIELDS.CHECK_IN.STUDIO_ID] &&
                        linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.SPORT_ITEM].value === mergedLines[j][constants.CUSTOM_FIELDS.CHECK_IN.SPORT_ITEM] &&
                        linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.BOOKING_NET_COST] === mergedLines[j][constants.CUSTOM_FIELDS.CHECK_IN.BOOKING_NET_COST] &&
                        linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.CLASS].value === mergedLines[j][constants.CUSTOM_FIELDS.CHECK_IN.CLASS] &&
                        linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.LOCATION_ID] === mergedLines[j][constants.CUSTOM_FIELDS.CHECK_IN.LOCATION_ID]) {
                        mergedLines[j][constants.CUSTOM_FIELDS.OTHER.QUANTITY]++;
                        lineFound = true;
                        break;
                    }
                }

                if (!lineFound) {
                    let lineData = {};
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.REGION_CODE_L] = linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.REGION_CODE_L].value;
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.STUDIO_ID] = linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.STUDIO_ID];
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.SPORT_ITEM] = linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.SPORT_ITEM].value;
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.BOOKING_NET_COST] = linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.BOOKING_NET_COST];
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.CLASS] = linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.CLASS].value;
                    lineData[constants.CUSTOM_FIELDS.CHECK_IN.LOCATION_ID] = linesParsedData[i][constants.CUSTOM_FIELDS.CHECK_IN.LOCATION_ID];
                    lineData[constants.CUSTOM_FIELDS.OTHER.QUANTITY] = 1;

                    mergedLines.push(lineData);
                }
            }

            return mergedLines;
        }

        /**
         * Iterates over an array of strings and returns array of objects
         * @param linesData
         * @returns [{object}]
         */
        const getLinesParsedData = (linesData) => {
            let linesParsedData = [];

            for (let i = 0; i < linesData.length; i++) {
                linesParsedData.push(JSON.parse(linesData[i]));
            }

            return linesParsedData;
        }

        /**
         * Iterates over search results and returns all Check-In IDs in the form of array
         * @param linesData
         * @returns [checkInIDs]
         */
        const getCheckInIDs = (linesData) => {
            let checkInIDs = [];

            for (let i = 0; i < linesData.length; i++) {
                checkInIDs.push(linesData[i][constants.CUSTOM_FIELDS.OTHER.CHECK_IN_ID]);
            }

            return checkInIDs;
        }

        /**
         * Compares Appointment Date's month against selected Date's month and return true if they are the same
         * @param appointmentDatetime
         * @returns boolean
         */
        const appointmentDateIsInSelectedMonth = (appointmentDatetime) => {
            let scriptObject = runtime.getCurrentScript();
            let selectedDate = scriptObject.getParameter(constants.SCRIPT_PARAMS.MR_GENERATE_TRANSACTIONS.DATE);

            let appointmentDateFormatted = format.parse({
                value: appointmentDatetime,
                type: format.Type.DATE
            });
            let selectedDateFormatted = format.parse({
                value: selectedDate,
                type: format.Type.DATE
            });

            return appointmentDateFormatted.getMonth() === selectedDateFormatted.getMonth() &&
                appointmentDateFormatted.getFullYear() === selectedDateFormatted.getFullYear();
        }

        return {getInputData, map, reduce}

    });
