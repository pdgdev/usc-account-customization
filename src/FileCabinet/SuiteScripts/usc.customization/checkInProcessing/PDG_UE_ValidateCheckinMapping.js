/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 */
define(['N/record', 'N/runtime', './CheckInMappingValidator', '../common/constants'],
    /**
 * @param{record} record
 * @param{runtime} runtime
 */
    (record, runtime, CheckInMappingValidator, constants) => {

        /**
         * Defines the function definition that is executed before record is submitted.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        const beforeSubmit = (scriptContext) => {
            let checkInRecord = scriptContext.newRecord;

            try {
                if (scriptContext.type !== scriptContext.UserEventType.CREATE && scriptContext.type !== scriptContext.UserEventType.EDIT) {
                    return;
                }

                let scriptObj = runtime.getCurrentScript();
                let merchantCategory = scriptObj.getParameter(constants.SCRIPT_PARAMS.UE_VALIDATE_MAPPING.MERCHANT_CATEGORY);

                let mappingValidator = new CheckInMappingValidator.Instance(checkInRecord)
                    .setMerchantCategory(merchantCategory)
                    .searchAndInitRecordFields()
                    .validateRecordFields()
                    .setCheckInFields();
            }
            catch (error) {
                log.error('<< beforeSubmit >>', error);
            }
        }

        return {beforeSubmit}

    });
